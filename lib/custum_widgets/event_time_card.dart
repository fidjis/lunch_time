import 'package:flutter/material.dart';
import 'package:lunch_time/models/time_event_model.dart';
import 'package:lunch_time/utilits/my_consts.dart';
import 'package:lunch_time/utilits/my_utils.dart';

class EventTimeCard extends StatefulWidget {

  final TimeEventModel timeEvent;

  EventTimeCard(this.timeEvent);

  @override
  _EventTimeCardState createState() => _EventTimeCardState();
}

class _EventTimeCardState extends State<EventTimeCard> {
  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (BuildContext context) {
        return Container(
          margin: EdgeInsets.all(5.0),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            child: Stack(children: <Widget>[
              Container(color: MyConsts.primaryColor,),
              Positioned(
                bottom: 0.0,
                right: 0.0,
                child: Image.asset(
                  'assets/images/clock_img2.png',
                  height: 140,
                  ),
              ),
              Positioned(
                top: 0.0,
                left: 0.0,
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        widget.timeEvent.eventName,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Divider(),
                      Text(
                        "Duração: ${MyUtils.formatTime(widget.timeEvent.eventTime)}",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        'Horario de Saida Padrão: ${MyUtils.formatTimeOfDay(widget.timeEvent.timeOutDefautSaida)}',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        'Horario de retorno Padrão: ${MyUtils.somarDuration(
                          vTimeOfDay: MyUtils.formatTimeOfDay(widget.timeEvent.timeOutDefautSaida),
                          vDuration: widget.timeEvent.eventTime)}',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 0.0,
                left: 0.0,
                right: 0.0,
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Color.fromARGB(200, 0, 0, 0), Color.fromARGB(0, 0, 0, 0)],
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                    ),
                  ),
                  padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        buildTextDayWeek('D', widget.timeEvent.domingo),
                        buildTextDayWeek('S', widget.timeEvent.segunda),
                        buildTextDayWeek('T', widget.timeEvent.terca),
                        buildTextDayWeek('Q', widget.timeEvent.quarta),
                        buildTextDayWeek('Q', widget.timeEvent.quinta),
                        buildTextDayWeek('S', widget.timeEvent.sexta),
                        buildTextDayWeek('S', widget.timeEvent.sabado),
                      ],
                    ),
                  ),
                ),
              ),
            ]),
          ),
        );
      },
    );
  }

  buildTextDayWeek(String string, bool isChecked) {
    return Text(
      string,
      style: TextStyle(
        color: isChecked ? Colors.white : Colors.grey[700],
        fontSize: 18.0,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}