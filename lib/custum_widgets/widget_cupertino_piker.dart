import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CupertinoPikerWidget extends StatefulWidget {
  @override
  _CupertinoPikerWidgetState createState() => _CupertinoPikerWidgetState();
}

Color secondaryColor = Colors.teal[900];
Color primaryColor = Colors.red[400];
int selectitem = 1;

//RETORNA UM FAB --- NAO ESTOU UTILIZANDO
class _CupertinoPikerWidgetState extends State<CupertinoPikerWidget> {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton.extended(
              onPressed: (){modal();},
              heroTag: "fab3",
              backgroundColor: Colors.white,
              icon: Icon(
                Icons.favorite,
                color: Colors.pink,
                size: 24.0,
              ),
              label: Text("Novo Horario", style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: secondaryColor,
                  fontSize: 17.0
              ))
      );
  }

  modal() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Scaffold(
              appBar: AppBar(
                title: Text(
                  "CupertinoPicker",
                  textAlign: TextAlign.justify,
                ),
                backgroundColor: Colors.teal,
                actions: <Widget>[
                  IconButton(
                    icon: Icon(Icons.send),
                    onPressed: () {},
                  )
                ],
              ),
              body: Container(
                child: CupertinoPicker(
                  magnification: 1.5,
                  backgroundColor: Colors.black87,
                  children: <Widget>[
                    Text(
                      "TextWidget",
                      style: TextStyle(
                          color: Colors.white, fontSize: 20),
                    ),
                    IconButton(
                      icon: Icon(Icons.home),
                      color: Colors.white,
                      iconSize: 40,
                      onPressed: () {},
                    )
                  ],
                  itemExtent: 50, //height of each item
                  looping: true,
                  onSelectedItemChanged: (int index) {
                    selectitem = index;
                  },
                ),
              ));
        });
  }

}