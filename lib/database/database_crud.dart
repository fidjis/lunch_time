import 'package:lunch_time/database/database_helper.dart';
import 'package:lunch_time/models/time_event_model.dart';

class DatabaseCRUD {
  // referencia nossa classe single para gerenciar o banco de dados
  final dbHelper = DatabaseHelper.instance;
  
  void inserir(TimeEventModel values) async {
    // linha para incluir
    Map<String, dynamic> row = {
      DatabaseHelper.columnId : values.eventID,
      DatabaseHelper.columnEventName : values.eventName,
      DatabaseHelper.columnEventTime  : values.eventTime,
      DatabaseHelper.columnTimeOutDefaut : values.timeOutDefautSaida,
      DatabaseHelper.columnDomingo : values.domingo,
      DatabaseHelper.columnSegunda : values.segunda,
      DatabaseHelper.columnTerca : values.terca,
      DatabaseHelper.columnQuarta : values.quarta,
      DatabaseHelper.columnQuinta : values.quinta,
      DatabaseHelper.columnSexta : values.sexta,
      DatabaseHelper.columnSabado : values.sabado
    };
    final id = await dbHelper.insert(row);
    print('linha inserida id: $id');
  }
  Future<List<TimeEventModel>> consultar() async {
    final todasLinhas = await dbHelper.queryAllRows();
    //todasLinhas.forEach((row) => print('$row\n'));
    List<TimeEventModel> events = new List<TimeEventModel>();
    todasLinhas.forEach((row){
      Map<String, dynamic> rowMap = row;
      events.add(new TimeEventModel(
        eventID: rowMap[DatabaseHelper.columnId].toString(),
        eventName: rowMap[DatabaseHelper.columnEventName],
        timeOutDefautSaida: rowMap[DatabaseHelper.columnTimeOutDefaut],
        eventTime: rowMap[DatabaseHelper.columnEventTime],
        domingo: rowMap[DatabaseHelper.columnDomingo].toString() == '1',
        segunda: rowMap[DatabaseHelper.columnSegunda].toString() == '1',
        terca: rowMap[DatabaseHelper.columnTerca].toString() == '1',
        quarta: rowMap[DatabaseHelper.columnQuarta].toString() == '1',
        quinta: rowMap[DatabaseHelper.columnQuinta].toString() == '1',
        sexta: rowMap[DatabaseHelper.columnSexta].toString() == '1',
        sabado: rowMap[DatabaseHelper.columnSabado].toString() == '1'
      ));
    });
    return events;
  }
  void atualizar(TimeEventModel event) async {
    // linha para atualizar
    Map<String, dynamic> row = {
      DatabaseHelper.columnId : int.parse(event.eventID),
      DatabaseHelper.columnEventName : event.eventName,
      DatabaseHelper.columnEventTime  : event.eventTime,
      DatabaseHelper.columnTimeOutDefaut : event.timeOutDefautSaida,
      DatabaseHelper.columnDomingo : event.domingo,
      DatabaseHelper.columnSegunda : event.segunda,
      DatabaseHelper.columnTerca : event.terca,
      DatabaseHelper.columnQuarta : event.quarta,
      DatabaseHelper.columnQuinta : event.quinta,
      DatabaseHelper.columnSexta : event.sexta,
      DatabaseHelper.columnSabado : event.sabado
    };
    final linhasAfetadas = await dbHelper.update(row);
    print('atualizadas $linhasAfetadas linha(s)');
  }
  void deletar(int row) async {
    // Assumindo que o numero de linhas é o id para a última linha
    //final id = await dbHelper.queryRowCount();
    final linhaDeletada = await dbHelper.delete(row);
    print('Deletada(s) $linhaDeletada linha(s): linha $row');
  }
  void deletarTudo() async {
    final todasLinhas = await dbHelper.queryAllRows();
    todasLinhas.forEach((row) async {
      Map<String, dynamic> rowMap = row;
      int id = rowMap[DatabaseHelper.columnId];
      await dbHelper.delete(id);
    });
    // Assumindo que o numero de linhas é o id para a última linha
    //final id = await dbHelper.queryRowCount();
    
  }
}