import 'dart:async';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lunch_time/ads/my_ads.dart';
import 'package:lunch_time/ads/target_info.dart';
import 'package:lunch_time/database/database_crud.dart';
import 'package:lunch_time/models/time_event_model.dart';
import 'package:lunch_time/utilits/my_utils.dart';

class CustumDialog extends StatefulWidget {

  final TimeEventModel eventParsed;

  CustumDialog(this.eventParsed);

  @override
  _CustumDialogState createState() => _CustumDialogState();
}

class _CustumDialogState extends State<CustumDialog> {

  bool isEditMode;
  MyAds adsBannerLarge01 = MyAds(TargetInfo.testBanner, AdmobBannerSize.LARGE_BANNER);
  Color colorTextDefaut = Colors.grey[700];
  Color colorTextError = Colors.red;
  Color colorEventTime = Colors.grey[700];
  Color colorTimeOutDefautSaida = Colors.grey[700];
  TimeEventModel event = new TimeEventModel();
  DatabaseCRUD db = new DatabaseCRUD();
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>(); //dialogprogress

  @override
  void initState() {
    super.initState();
    if(widget.eventParsed != null){
      event = widget.eventParsed;
      isEditMode = true;
    }else
      isEditMode = false;

    zones = [
      Zone('domingo', 'D', isEditMode ? widget.eventParsed.domingo : false),
      Zone('segunda', 'S', isEditMode ? widget.eventParsed.segunda: true),
      Zone('terca', 'T', isEditMode ? widget.eventParsed.terca : true),
      Zone('quarta', 'Q', isEditMode ? widget.eventParsed.quarta : true),
      Zone('quinta', 'Q', isEditMode ? widget.eventParsed.quinta : true),
      Zone('sexta', 'S', isEditMode ? widget.eventParsed.sexta : true),
      Zone('sabado', 'S', isEditMode ? widget.eventParsed.sabado: false)
    ];
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.red,
        title: Text(isEditMode ? widget.eventParsed.eventName : 'Novo Intervalo'),
        actions: [
          new FlatButton(
            onLongPress: (){
              db.deletarTudo();
            }, //teste
              onPressed: () async {
                bool validateEventTimeBool = validateEventTime();
                bool validadeTimeOutDefautSaidaBool = validadeTimeOutDefautSaida();
                if (_formKey.currentState.validate() && validateEventTimeBool && validadeTimeOutDefautSaidaBool) {
                  _formKey.currentState.save();

                  showLoadingDialog(context, _keyLoader); //chamando progressdialog
                  
                  if(isEditMode)
                    db.atualizar(event);
                  else
                    db.inserir(event);

                  Timer(const Duration(milliseconds: 1000), () {
                    Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                    Navigator.of(context).pop(event);
                  });
                  
                }
              },
              child: new Text(isEditMode? 'ATUALIZAR' : 'SALVAR',
                  style: Theme
                      .of(context)
                      .textTheme
                      .subhead
                      .copyWith(color: Colors.white))),
        ],
      ),
      body: SingleChildScrollView(child: buildForm(),),
    );
  }
  
  buildForm(){
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          SizedBox(height: 50,),
          Card(
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: TextFormField(
                initialValue: event.eventName,
                decoration: InputDecoration(labelText: 'Nome do Intervalo'),
                validator: (value){
                  if (value.isEmpty) {
                      return ''; //vai so deixar em vermelho - sem mensagem
                  }
                },
                onSaved: (val) => setState(() => event.eventName = val),
              )
            ),
          ),
          Card(
            child: ListTile(
              leading: new Icon(
                Icons.timer,
                size: 30,
                color: Colors.grey[500],
              ),
              title: new Text(
                "Horario do Intervalo: ${MyUtils.formatTimeOfDay(event.timeOutDefautSaida)}",
                style: TextStyle(
                  color: colorTimeOutDefautSaida,
                ),
              ),
              onTap: () async {
                TimeOfDay time = await showTimePicker(context: context, initialTime: TimeOfDay.now());
                if(time != null)
                  setState(() {
                    event.timeOutDefautSaida = time.toString(); //TODO: Inplemtar para aparecer h e m
                  });
                validadeTimeOutDefautSaida();
                //Scaffold.of(context).showSnackBar(new SnackBar(content: new Text("Chose duration: $resultingDuration")));
              },
            ),
          ),
          Card(
            child: ListTile(
              leading: new Icon(
                Icons.timelapse,
                size: 30,
                color: Colors.grey[500],
              ),
              title: new Text(
                "Tempo do Intervalo: ${MyUtils.formatTime(event.eventTime)}",
                style: TextStyle(
                  color: colorEventTime,
                ),
              ),
              onTap: (){
                _buildCupertinoPiker();
                validateEventTime();
                //Scaffold.of(context).showSnackBar(new SnackBar(content: new Text("Chose duration: $resultingDuration")));
              },
            ),
          ),
          Card(
            //color: Colors.lightBlueAccent,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text('Dias da Semana:'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: buildCircleDayButtons(context, zones),
                ),
                SizedBox(height: 20,)
              ],
            ),
          ),
          adsBannerLarge01,
        ],
      ),
    );
  }

  validateEventTime(){
    if(event.eventTime == null){
      setState(() {
        colorEventTime = colorTextError;
      });
      return false;
    }else{
      setState(() {
        colorEventTime = colorTextDefaut;
      });
      return true;
    }
  }
  
  validadeTimeOutDefautSaida(){
    if(event.timeOutDefautSaida == null){
      setState(() {
        colorTimeOutDefautSaida = colorTextError;
      });
      return false;
    }else{
      setState(() {
        colorTimeOutDefautSaida = colorTextDefaut;
      });
      return true;
    }
  }

  Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(height: 10,),
                        Text("Salvando....",style: TextStyle(color: Colors.blueAccent),)
                      ]),
                    )
                  ]));
        });
  }

  _buildCupertinoPiker(){
    showModalBottomSheet<void>(context: context,
      builder: (BuildContext context) {
        return Container(
          color: Colors.white,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              CupertinoTimerPicker(
                backgroundColor: Colors.white,
                mode: CupertinoTimerPickerMode.hms,
                minuteInterval: 1,
                initialTimerDuration: Duration.zero,
                onTimerDurationChanged: (Duration changedtimer) {
                  setState(() {
                    event.eventTime = changedtimer.toString();
                    validateEventTime();
                  });
                },
              ),
            ],
          ),
        );
      }
    );
  }

  ///logica dos botoes dos dias da semana
  List<Zone> zones;
  buildCircleDayButtons(BuildContext context, List<Zone> zones) {
    return zones.map((Zone zone) {
      pupolateEnventDayWeek(zone.key, zone.isSelected); //setenda na var que sera salva do DB
      return Padding(
      padding: const EdgeInsets.all(1.0),
      child: FittedBox(
        fit: BoxFit.scaleDown,
        child: SizedBox(
          height: 40,
          width: 40,
          child: RaisedButton(
            child: Text(
              zone.text,
              style: TextStyle(fontSize: 13),
            ),
            shape: StadiumBorder(),
            color: zone.isSelected ? Colors.blueAccent : Colors.grey,
            onPressed: (){
              setState(() {
                zone.isSelected = !zone.isSelected;//vai trocar o valor pelo contrario
              });
              pupolateEnventDayWeek(zone.key, zone.isSelected);
            }, 
          ),
        ),
      ),
    );
    }).toList();
  }

  pupolateEnventDayWeek(String weekDay, bool value){
    switch (weekDay) {
      case 'domingo':
        event.domingo = value;
        break;
      case 'segunda':
        event.segunda = value;
        break;
      case 'terca':
        event.terca = value;
        break;
      case 'quarta':
        event.quarta = value;
        break;
      case 'quinta':
        event.quinta = value;
        break;
      case 'sexta':
        event.sexta = value;
        break;
      case 'sabado':
        event.sabado = value;
        break;
      default:
    }
  }
}

class Zone {
  String key; //dia da semana completo
  String text;
  bool isSelected;

  Zone(this.key, this.text, this.isSelected);
}

class Consts {
  Consts._();
  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}