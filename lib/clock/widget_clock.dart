
import 'dart:async';
import 'dart:developer';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lunch_time/clock/play_state.dart';
import 'package:lunch_time/clock/timer_painter.dart';
import 'package:lunch_time/custum_widgets/widget_show_up.dart';
import 'package:lunch_time/dialog/notification.dart';
import 'package:lunch_time/models/time_event_model.dart';
import 'package:lunch_time/utilits/my_consts.dart';
import 'package:lunch_time/utilits/my_utils.dart';

class ClockWidget extends StatefulWidget {

  setEvent(TimeEventModel eventModel){
    myState.setEvent(eventModel);
    myState.setTime(MyUtils.parseDuration(eventModel.eventTime));
  }

  final _ClockWidgetState myState = _ClockWidgetState();

  @override
  //_ClockWidgetState createState() => _ClockWidgetState();
  _ClockWidgetState createState() {
    return myState;
  }
}

class _ClockWidgetState extends State<ClockWidget> with TickerProviderStateMixin {
  AnimationController animationController;
  TimeEventModel eventModel;
  Duration time;
  bool isTimeActive = false;
  MyNotification myNotification;

  setTime(Duration time){
    setState(() {
      if(!isTimeActive){//se nao tiver contando o tempo
        this.time = time;
        animationController = AnimationController(vsync: this, duration: time);
      } 
    });
  }

  setEvent(TimeEventModel eventModel){
    setState(() {
      if(!isTimeActive){
        this.eventModel = eventModel;
      } 
    });
  }

  String get timerString {
    Duration duration = animationController.duration * animationController.value;
    if(duration.inSeconds == 1 && MyConsts.playState == PlayState.stopped){
      playSound();
      myNotification.showNotification();
      setState(() {
        _visible = false;
        _visibleAudioButton  = true; 
      });
    }
    return '${duration.inMinutes}:${(duration.inSeconds % 60)
        .toString()
        .padLeft(2, '0')}';
  }

  @override
  void initState() {
    super.initState();
    //time = widget.timeInitial;
    MyConsts.playState = PlayState.stopped;
    time = Duration();
    _audioCache = AudioCache(prefix: "audio/", fixedPlayer: advancedPlayer);
    animationController = AnimationController(vsync: this, duration: time);
    myNotification = new MyNotification(context);
  }

  ///AUDIO
  //bool isPlayng = false;
  AudioCache _audioCache;
  AudioPlayer advancedPlayer = new AudioPlayer();
  double volume = 0.1;
  Future<void> playSound() async {
    //_audioCache.play('sino2.wav');
    _audioCache.loop('sino2.wav', volume: volume);
    //isPlayng = true;
    MyConsts.playState = PlayState.plaing;
    log('Play Audio');

    new Timer(const Duration(seconds: 3), () {
      setState(() {
        //advancedPlayer.setVolume(1);
      });
    });
    new Timer(const Duration(seconds: 20), () {
      setState(() {
        advancedPlayer.stop();
        //isPlayng = false;
        _visibleAudioButton  = false;
        MyConsts.playState = PlayState.stopped;
        log('Stop Audio');
      });
    });

    while (volume < 1) {
      await Future.delayed(const Duration(milliseconds: 2500), () {
        setState(() {
          //if(isPlayng){
          if(MyConsts.playState == PlayState.plaing){
            volume+= 0.1;
            log('volume = $volume');
            advancedPlayer.setVolume(volume);
          }else{
            volume = 0.1;
          }
        });
      });
      if(volume == 0.1) //ativado para nao gerar multiplos whiles
        break;
    }

    //await audioPlayer.play("assets/audio/sino2.wav");
    //setState(() => playerState = PlayerState.playing);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 290,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            top: 0.0,
            child: Container(
              height: 260,
              width: MediaQuery.of(context).size.width,
              child: AppBar(
                backgroundColor: Colors.red[400],
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                    bottom: Radius.circular(50),
                  ),
                ),
                bottom: PreferredSize(
                    preferredSize: Size(0.0, 260),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox(height: 5,),
                        buildCircleAnimated(),
                        SizedBox(height: 30,)
                      ],
                    )
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0.0,
            child: buildButtons()
          )
        ],
      ),
    );
  }

  buildCircleAnimated() {
    return Container(
      height: 225,
      child: Align(
        alignment: FractionalOffset.center,
        child: AspectRatio(
          aspectRatio: 1.0,
          child: Stack(
            children: <Widget>[
              Positioned.fill(
                child: AnimatedBuilder(
                  animation: animationController,
                  builder: (BuildContext context, Widget child) {
                    return CustomPaint(
                      painter: TimerPainter(
                        animation: animationController,
                        backgroundColor: Colors.black26,
                        color: Colors.white
                      ),
                    );
                  },
                ),
              ),
              buildInfTimeCont(context)
            ],
          ),
        ),
      ),
    );
  }
  
  bool _visible = false;
  bool _visibleAudioButton = false;
  buildButtons(){
    return Container(margin: EdgeInsets.all(8.0), child: Row(
      //mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        
        Container(
          width: 50,
          child: Visibility( 
            visible: _visible,
            child: ShowUp(delay: 200, 
              child: FloatingActionButton(
                heroTag: "fab005",
                onPressed: (){
                  setState(() {
                    myNotification.cancelNotification();
                    animationController.stop();
                    _visible = !_visible;
                  });
                },
                backgroundColor: Colors.deepOrange[900],
                mini: true,
                child: Icon(Icons.stop),
              )
            )
          ),
        ),
        //BOTAO DO MEIO
        Container(
          child: FloatingActionButton.extended(
            onPressed: () {
              //TODO: nao esquecer de colocar condicao para so tocar quando o Duration for diferente de null
              if (animationController.isAnimating) {
                animationController.stop();
              } else {
                animationController.reverse(from: animationController.value == 0.0 ? 1.0 : animationController.value);
                setState(() {
                  _visible = !_visible;
                });
              }
            },
            backgroundColor: Colors.deepOrange[900],
            label: AnimatedBuilder(
              animation: animationController,
              builder: (_, Widget child) {
                return Icon(animationController.isAnimating
                    ? Icons.pause
                    : Icons.play_arrow, color: Colors.white,
                );
              }),
          ),
        ),
        Container(
          width: 50,
          child: Visibility( 
            visible: _visibleAudioButton,
            child: ShowUp(delay: 200, 
              child: FloatingActionButton(
                heroTag: "fab007",
                onPressed: (){
                  setState(() {
                    myNotification.cancelNotification();
                    advancedPlayer.stop();
                    _visibleAudioButton = !_visibleAudioButton;
                  });
                },
                backgroundColor: Colors.deepOrange[900],
                mini: true,
                child: Icon(Icons.alarm_off),
              )
            )
          ),
        ),
        //SizedBox(width: 50,)

      ],
    ),);
  }
  
  buildInfTimeCont(BuildContext context) {
    return Align(
      alignment: FractionalOffset.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          AnimatedBuilder(
            animation: animationController,
            builder: (_, Widget child) {
              return Text(
                timerString,
                style: TextStyle(
                  fontSize: 50
                ),
              );
            }
          ),
          Text(
            eventModel == null ? "---" : "volta: " + MyUtils.somarDuration(
              vTimeOfDay: MyUtils.formatTimeOfDay(TimeOfDay.now().toString()), //manda o horario que saiu 
              vDuration: eventModel.eventTime
            ),
            style: TextStyle(
              fontSize: 13
            ),
          ),
          Text(
            eventModel != null ? eventModel.eventName : "--",
            style: TextStyle(
              fontSize: 11,
            ),
          ),
        ],
      ),
    );
  }
}