import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:lunch_time/ads/target_info.dart';
import 'package:lunch_time/screens/home_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Admob.initialize(TargetInfo.idAppAds); //ads inicializando
    return MaterialApp(
      title: 'Lunch Time',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
      routes: <String, WidgetBuilder> {
        '/home_screen': (BuildContext context) => new HomeScreen(), //new
      },
    );
  }
}
