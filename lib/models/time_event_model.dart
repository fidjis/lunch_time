class TimeEventModel{
  
  TimeEventModel({
    this.eventID,
    this.eventName,
    this.timeOutDefautSaida,
    this.eventTime,
    this.domingo,
    this.segunda,
    this.terca,
    this.quarta,
    this.quinta,
    this.sexta,
    this.sabado
  });

  String eventID;
  String eventName;
  String timeOutDefautSaida;
  String eventTime;
  bool domingo;
  bool segunda;
  bool terca;
  bool quarta;
  bool quinta;
  bool sexta;
  bool sabado;
  
}