import 'package:flutter/material.dart';
import 'package:lunch_time/clock/play_state.dart';

class MyConsts{
  static PlayState playState;
  static Color secondaryColor = Colors.teal[900];
  static Color primaryColor = Colors.red[400];
}
