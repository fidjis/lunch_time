import 'package:flutter/material.dart';

class MyUtils{

  //convertendo string para Duration 
  static Duration parseDuration(String s) {
    int hours = 0;
    int minutes = 0;
    int micros;
    List<String> parts = s.split(':');
    if (parts.length > 2) {
      hours = int.parse(parts[parts.length - 3]);
    }
    if (parts.length > 1) {
      minutes = int.parse(parts[parts.length - 2]);
    }
    micros = (double.parse(parts[parts.length - 1]) * 1000000).round();
    return Duration(hours: hours, minutes: minutes, microseconds: micros);
  }

  static somarDuration({@required String vTimeOfDay, @required String vDuration}){
    //tem que receber o vTimeOfDay ja formatado
    TimeOfDay timeOfDay = new TimeOfDay(
      minute: int.parse(vTimeOfDay.substring(vTimeOfDay.indexOf(":") + 1)),
      hour: int.parse(vTimeOfDay.substring( 1 , vTimeOfDay.indexOf(":"))),
    );
    Duration duration = parseDuration(vDuration);
    print(timeOfDay.toString());
    print((duration.inMinutes / 60).round());

    Duration finalDuration = new Duration(
      hours: duration.inHours + timeOfDay.hour,
      //minute: (duration.inMinutes / 60).round() + timeOfDay.minute
      minutes: duration.inMinutes  + timeOfDay.minute
    );

    return '${finalDuration.inHours}:${(finalDuration.inMinutes % 60)
        .toString()
        .padLeft(2, '0')}';
  }

  static formatTimeOfDay(String time){
    if(time != null){
      const start = "(";
      const end = ")";
      final startIndex = time.indexOf(start);
      final endIndex = time.indexOf(end, startIndex + start.length);
      return time.substring(startIndex + start.length, endIndex); 
    }else
      return "";
  }

  static formatTime(String time){
    if(time != null){
      final endIndex = time.indexOf(".");
      return "0" + time.substring(0, endIndex - 3); 
    }else
      return "";
  }
}