
import 'package:admob_flutter/admob_flutter.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:lunch_time/ads/my_ads.dart';
import 'package:lunch_time/ads/target_info.dart';
import 'package:lunch_time/clock/play_state.dart';
import 'package:lunch_time/clock/widget_clock.dart';
import 'package:lunch_time/custum_widgets/event_time_card.dart';
import 'package:lunch_time/database/database_crud.dart';
import 'package:lunch_time/dialog/custum_dialog.dart';
import 'package:lunch_time/models/time_event_model.dart';
import 'package:lunch_time/utilits/my_consts.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

ClockWidget clockWidget = ClockWidget();

class _HomeScreenState extends State<HomeScreen> {

  List<TimeEventModel> events = new List<TimeEventModel>();
  String nomeEventAtual = "";
  String tempoEventAtual = "";
  DatabaseCRUD db = new DatabaseCRUD();
  MyAds adsBanner01;
  MyAds adsBannerLarge01;

  @override
  void initState() {
    super.initState();
    populaterTimeEvents();
    MyConsts.playState = PlayState.stopped;
    //adsBanner01 = MyAds(TargetInfo.testBanner, AdmobBannerSize.BANNER);
    adsBanner01 = MyAds(TargetInfo.idBanner01, AdmobBannerSize.BANNER);
    //adsBannerLarge01 = MyAds(TargetInfo.testBanner, AdmobBannerSize.LARGE_BANNER);
    adsBannerLarge01 = MyAds(TargetInfo.idBanner02, AdmobBannerSize.LARGE_BANNER);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: adsBanner01,
        backgroundColor: Colors.red[400],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: (){
          showCustumDialog();
        },
        heroTag: 'dialog1',
        backgroundColor: MyConsts.primaryColor,
        label: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            //Container(height: 320, child: Container(child: clockWidget,)),
            clockWidget,
            buildText(),
            Divider(),
            getCarrossel(),
            Divider(),
            adsBannerLarge01
          ],
        ),
      ),
    );
  }

  //Texto do card selecionado --- Mudar nome do metodo
  buildText() {
    return Align(
      alignment: FractionalOffset.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            "$nomeEventAtual",
            //style: Theme.of(context).textTheme.subhead,
            style: TextStyle(
              fontSize: 25,
            ),
          ),
          Container(
            color: MyConsts.secondaryColor,
            height: 2, 
            width: MediaQuery.of(context).size.width / 2.5),
          Text(
            "$tempoEventAtual",
            //style: Theme.of(context).textTheme.subhead,
            style: TextStyle(
              fontSize: 15,
            ),
          ),
        ],
      ),
    );
  }

  //mudar informações do card quando tiver ativado o tempo do evento - contador
  getCarrossel(){
    return FutureBuilder<List<TimeEventModel>>(
      //initialData: events,
      future: db.consultar(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if(snapshot.hasError)
          return Center(
            child: Text("Ocorreu um erro ao carregar os dados"),
          );
        else if (!snapshot.hasData)
          return Center(
            child: CircularProgressIndicator(),
          );
        else if(snapshot.data.length == 0)
          return Center(
            //padding: const EdgeInsets.only(top: 80),
            child: Text(
              'Adicione seus horarios!',
              style: TextStyle(
                  fontSize: 16.0,
              ),
            ),
          );
        else{
          events = snapshot.data;
          //FAZENDO O WIDGET DOS TEXTOS APARECER
          //clockWidget.setTime(parseDuration(events[0].eventTime));
          //nomeEventAtual = events[0].eventName;
          //tempoEventAtual = events[0].eventTime.substring(0, events[0].eventTime.indexOf("."));
          return getCarrosselCERTO();
          
        }
      },   
    );
  }

  getCarrosselCERTO(){
    CarouselSlider carousel = CarouselSlider(
      //height:  MediaQuery.of(context).size.height * 0.25,
      height:  200,
      enlargeCenterPage: true,
      onPageChanged: (pageID){
        setInfos(pageID);
      },
      initialPage: 0,
      items: events.map((final event) {
        return GestureDetector(
          onLongPress: (){
            //menu popUp para deletar
          },
          onTap: (){
            showCustumDialog(event: event);
          },
          child: EventTimeCard(event),
        );
      }).toList(),
    );

    //carousel.items.add(getBanner());

    return carousel;
  }

  populaterTimeEvents() async {
    events = await db.consultar();
    if(events.isNotEmpty)
      setInfos(0); //setando o primeiro clock
  }

  //Quando receber o event como parametro, ele inicia o processo de edicao
  Future showCustumDialog({TimeEventModel event}) async {
    TimeEventModel result = await Navigator.of(context).push(new MaterialPageRoute<TimeEventModel>(
      builder: (BuildContext context) {
        return new CustumDialog(event);
      },
      fullscreenDialog: true
    ));
    if (result != null) {
      setState(() {
        events.add(result);
        //nomeEventAtual = result.eventName;
        clockWidget.setEvent(result);
        //tempoEventAtual = result.eventTime.substring(0, result.eventTime.indexOf("."));
      });
    }
  }

  void setInfos(int pageID) {
    setState(() {
      if(MyConsts.playState == PlayState.stopped)
        clockWidget.setEvent(events[pageID]);

      nomeEventAtual = events[pageID].eventName;
      tempoEventAtual = events[pageID].eventTime.substring(0, events[pageID].eventTime.indexOf("."));
    });
  }
}