import 'dart:async';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';

class MyAds extends StatefulWidget{

  final adsID;
  final adsSize;

  MyAds(this.adsID, this.adsSize);

  @override
  _MyAdsState createState() => _MyAdsState();
}

class _MyAdsState extends State<MyAds> {

  int cont = 10;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AdmobBanner(
        adUnitId: widget.adsID,
        adSize: widget.adsSize,
        listener: (AdmobAdEvent event, Map<String, dynamic> args) {
          switch (event) {
            case AdmobAdEvent.loaded:
              print('Admob banner loaded!');
              new Timer(Duration(seconds: cont), () {
                try {
                  setState(() => print('try reload!'));
                  if(cont < 380) //ate x minutos volta a contagem
                    cont += cont;
                  else 
                    cont = 10;
                } catch (e) {
                  print('Exeption: $e');
                }
              });
              break;

            case AdmobAdEvent.opened:
              print('Admob banner opened!');
              break;

            case AdmobAdEvent.closed:
              print('Admob banner closed!');
              break;

            case AdmobAdEvent.failedToLoad:
              print('Admob banner failed to load. Error code: ${args['errorCode']}');
              try {
                setState(() => cont = 10);
              } catch (e) {
                print('Exeption: $e');
              }
              break;
            case AdmobAdEvent.clicked:
              print('Admob banner clicked!');
              break;
            case AdmobAdEvent.impression:
              print('Admob banner impression!');
              break;
            case AdmobAdEvent.leftApplication:
              print('Admob banner leftApplication!');
              break;
            case AdmobAdEvent.completed:
              print('Admob banner completed!');
              break;
            case AdmobAdEvent.rewarded:
              print('Admob banner rewarded!');
              break;
            case AdmobAdEvent.started:
              print('Admob banner started!');
              break;
          }
        }
      ),
    );
  }

}